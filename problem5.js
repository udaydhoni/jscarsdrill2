let inventory = require('./inventory.js');
let years = require('./problem4.js');
let data = years(inventory);
function carsOlderThan(y) {
    let condensedData = data.filter(year => year<y);
    let reqCarData = [];
    reqCarData = inventory.filter(car => car["car_year"] == year);
    return [reqCarData.length,reqCarData];
}

module.exports = { carsOlderThan, inventory , years , data};

