function years(inventory) {
    let reqYearData = [];
    inventory.map(car => reqYearData.push(car["car_year"]));
    return reqYearData;
}
module.exports = years;