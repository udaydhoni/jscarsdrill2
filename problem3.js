function sortAlphabetically (inventory) {
    let namesOfCars = inventory.map(car => car["car_make"]);
    let sortedNames = namesOfCars.sort();
    let sortedInventory = [];
    sortedNames.map(carName => sortedInventory.push(...inventory.filter(car => car["car_make"] == carName)));
    return sortedInventory;
}

module.exports = sortAlphabetically;